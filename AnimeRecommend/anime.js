import fetch from "node-fetch"


const url = "https://api.jikan.moe/v4/recommendations/anime";
const url2 = "https://api.jikan.moe/v4/anime/{id}"

const getTitles = async () => {
  try {
    const response = await fetch(url);
    const data = await response.json();
    
    // Extract titles 
    const titles = data.data.map(entry => entry.entry.map(t => t.title)).flat().slice(0, 20);
    console.log(titles)
  } catch (error) {
    console.log("Error:", error);
  }
};

const getId = async () => {
  try {
    const response = await fetch(url);
    const data = await response.json();

    // Extract Id 
    const ids = data.data.map(entry => entry.entry.map(e => e.mal_id)).flat().slice(0, 20);

    for (let i=0;i<=ids.length;++i){
      const id = ids[i]
      const animeUrl = url2.replace("{id}", id)
      console.log(animeUrl)

      try {
    const animeResponse = await fetch(animeUrl)
    const animeData = await animeResponse.json()
    const aired = animeData.data.aired

    console.table(aired)
}
catch (error) {
  console.log("Error fetching anime data:", error)
}
     
    }

    console.log(ids);
  }
  catch (error) {
    console.log('Failed to get id:', error);
  }
}

// const getDate = async () => {
//   try{
//     const response = await fetch(url);
//     const data = await response.json();  
//   }
//   catch (error) {
//     console.log(`Failed to retrieve date: ${error}`);
//   }
// }

getTitles()
getId()




